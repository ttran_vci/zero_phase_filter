// BiDirFilter.cpp : Defines the entry point for the console application.
//



#include "stdafx.h"
#include "my_data_types.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#define FILTER_OUT_SHIFT 15
#define FIR_TAPS 18
#define ECG_REV_DIR_DEPTH       320
#define MOD_MASK                (ECG_REV_DIR_DEPTH-1)
static __int32 iir_ecgs[ECG_REV_DIR_DEPTH] = {0};
static __int16 wr_idx = (ECG_REV_DIR_DEPTH-1);

const __int16 fb[FIR_TAPS/2]={111, -413, 711, -483, -697, 2334, -2691, -936, 18446};// d=fdesign.lowpass('Fp,Fst,Ap,Ast',0.52,0.8,0.01,30)
const __int64 ib[2] = {32361, -32361};  // [B, A] = butter(1, 0.5/62.5, 'high')
const __int64 ia[2] = {32768, -31955};
__int16 ecg_bidir_filter(__int16 *ecg_in, __int16 num_samples);

using namespace std;

__int16 ecg_out[19];
int _tmain(int argc, _TCHAR* argv[])
{
    int idx = 0;
    int16 ecg_in;
    __int16 batch_ecg[19];
    int batch_count;

    ifstream ifs;
    ofstream ofs;
    string line;

    if(argc < 2)
    {
        cout << "Missing input argument" << endl;
        return -1;
    }

    std::string filename = argv[1];
    int32_t ext_pos = filename.find_last_of('.');
    ifs.open(filename);
    if ( !ifs )
    {
        cout << "Fail to open file: " ;
        cout << filename.c_str() << "please check the path and filename for existing\n";
        return -1;
    }

    for(int i = 0; i < ECG_REV_DIR_DEPTH; ++i)
    {
        iir_ecgs[i] = i;
    }
    int index = 0;

    while(getline(ifs,line))
    {
        stringstream ss(line);
        ss >> ecg_in;
        batch_ecg[index++] = ecg_in;
        if(index == 19)
        {
            int num_out = ecg_bidir_filter(batch_ecg, index);
            for(int i = 0; i < num_out; ++i)
            {
                cout << ecg_out[i] << endl;
            }
            index = 0;
        }
    }
    int num_out = ecg_bidir_filter(batch_ecg, index);
    for(int i = 0; i < num_out; ++i)
    {
        cout << ecg_out[i] << endl;
    }
    ifs.close();
	return 0;
}


__int16 ecg_bidir_filter(__int16 *ecg_in, __int16 num_samples)
{
  static __int16 fir_in[FIR_TAPS] = {0};
  static long long iir_in = 0;
  static long long iir_out = 0;
  long long iir_rev_out = 0;
  long long iir_rev_temp = 0;
  long long fir_out_0=0;
 
  __int16 ii, jj;
  __int16 start_idx = wr_idx;
  long long fir_out;



  /* reverse direction IIR filtering with depth = ECG_REV_DIR_DEPTH */
  iir_rev_temp = iir_ecgs[start_idx];
  for(ii = 0; ii < (ECG_REV_DIR_DEPTH - num_samples) ;++ii)
  {
    jj = (start_idx + ECG_REV_DIR_DEPTH - ii) %  ECG_REV_DIR_DEPTH;
    iir_rev_out = (ib[0]*iir_ecgs[jj] + ib[1]*iir_rev_temp - ia[1]*iir_rev_out) >> FILTER_OUT_SHIFT;
    iir_rev_temp = iir_ecgs[jj];
  }

  /* reverse direction IIR filtering with depth = ECG_REV_DIR_DEPTH */
  for(; ii < ECG_REV_DIR_DEPTH; ii++)
  {
    jj = (start_idx + ECG_REV_DIR_DEPTH - ii) %  ECG_REV_DIR_DEPTH;
    iir_rev_out = (ib[0]*iir_ecgs[jj] + ib[1]*iir_rev_temp - ia[1]*iir_rev_out) >> FILTER_OUT_SHIFT;
    iir_rev_temp = iir_ecgs[jj];
    ecg_out[ECG_REV_DIR_DEPTH - ii - 1] = iir_rev_out;
  }

  for(jj = 0; jj < num_samples; jj++)
  {
      fir_out = 0;
      for(ii=FIR_TAPS-1; ii>0; ii--)
      {
        fir_in[ii] = fir_in[ii-1];
      }
      fir_in[0] = ecg_in[jj];

      for(ii=0; ii<FIR_TAPS/2; ii++)  // EC13 filter
      {
        fir_out += fb[ii] * (fir_in[ii] + fir_in[FIR_TAPS-1-ii]);
      }
      fir_out = fir_out >> FILTER_OUT_SHIFT;
      
  /* hi pass filter */
      iir_out = (ib[0]*fir_out + ib[1]*iir_in - ia[1]*iir_out) >> FILTER_OUT_SHIFT;
      iir_in = fir_out;
    wr_idx = (wr_idx + 1) % ECG_REV_DIR_DEPTH;
    iir_ecgs[wr_idx] = iir_out;
  }
  

  return num_samples;
}