#pragma once

#define uint16_t    unsigned short
#define int16_t     signed short
#define uint32_t    unsigned int
#define int32_t     signed int
#define uint8_t     unsigned char
#define int8_t      signed char

#define uint16    unsigned short
#define int16     signed short
#define uint32    unsigned int
#define int32     signed int
#define uint8     unsigned char
#define int8      signed char
